<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create("fr_FR");

        for($i = 1; $i <= 3; $i++) {
            $category = new Category();
            $category->setTitle($faker->sentence());

            $manager->persist($category);

            for($a = 1; $a <= mt_rand(2, 4); $a++) {
            $article = new Article();

                $content = "<p>" . join($faker->paragraphs(5), "</p><p>") . "</p>";

                $article->setTitle($faker->sentence())
                        ->setResume($faker->paragraph())
                        ->setContent($content)
                        ->setImage($faker->imageUrl())
                        ->setCreatedAt($faker->dateTimeBetween("- 3 mouths"))
                        ->setCategory($category);

                $manager->persist($article);

                for($b = 1; $b <= mt_rand(1, 3); $b++) {
                    $comment = new Comment();

                    $content = "<p>" . join($faker->paragraphs(2), "</p><p>") . "</p>";

                    $days = (new \DateTime())->diff($article->getCreatedAt())->days;

                    $comment->setAuthor($faker->name())
                            ->setContent($content)
                            ->setEmail($faker->email())
                            ->setCreatedAt($faker->dateTimeBetween("-" . $days . " days"))
                            ->setArticle($article);

                    $manager->persist($comment);
                }
            }
        }

        

        $manager->flush();
    }
}
