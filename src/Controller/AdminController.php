<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\RegistrationType;
use App\Entity\Admin;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminController extends Controller
{
    // /**
    //  * @Route("/inscription", name="admin_inscription")
    //  */
    // public function registration(Request $req, ObjectManager $manager, UserPasswordEncoderInterface $encoder) {

    //     $admin = new Admin();

    //     $form = $this->createForm(RegistrationType::class, $admin);

    //     $form->handleRequest($req);

    //     if($form->isSubmitted() && $form->isValid()) {
    //         $hash = $encoder->encodePassword($admin, $admin->getPassword());

    //         $admin->setPassword($hash);

    //         $manager->persist($admin);
    //         $manager->flush();

    //         return $this->redirectToRoute("admin_login", []);
    //     }

    //     return $this->render("registration.html.twig", ["formRegistration" => $form->createView()]);
    // }

    /**
     * @Route("/login", name="admin_login")
     */
    public function login(AuthenticationUtils $authenticationUtils) {

        $error = $authenticationUtils->getLastAuthenticationError();

        return $this->render("connexion.html.twig", ["error" => $error]);
    }

    /**
     * @Route("/logout", name="admin_logout")
     */
    public function logout() {}

    /**
     * @Route("/admin", name="admin_page")
     */
    public function adminPage() {
        return $this->render("admin.html.twig", []);
    }
}
