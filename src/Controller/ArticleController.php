<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Form\CommentType;
use App\Entity\Comment;
use App\Service\FileUploader;

class ArticleController extends Controller {

    /**
     *  @Route("/", name="homepage")
     */
    public function index(CategoryRepository $categories, ArticleRepository $repo, Request $request){

        $articles = $repo->findAll();

        $categories = $categories->findAll();

        if($request->isMethod('POST')) {

            $category = $request->get("category");

            $articles = $repo->findBy(['category' => $category]);
        }

        return $this->render("index.html.twig", ["articles" => $articles, "categories" => $categories, "imageURI" => $this->getParameter('images_URI')]);
    }

    /**
     *  @Route("/admin/create-article", name="create_article")
     *  @Route("/admin/{id}/edit", name="edit_article")
     */
    public function form(Article $article = null, Request $req, ObjectManager $manager, FileUploader $fileUploader){

        if(!$article) {
            $article = new Article();
        }

        $form = $this->createFormBuilder($article)
                     ->add("title")
                     ->add("resume", TextareaType::class)
                     ->add("image", FileType::class)
                     ->add("content", TextareaType::class)
                     ->add("category", EntityType::class, [
                         "class" => Category::class,
                         "choice_label" => "title"
                     ])
                     ->getForm();

        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$article->getId()) {
            $article->setCreatedAt(new \DateTime());
            }

            $file = $article->getImage();

            $fileName = $fileUploader->upload($file);

            $article->setImage($fileName);

            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute("show_article", ["id" => $article->getId()]);
        }
        
        return $this->render("newArticle.html.twig", ["formArticle" => $form->createView(),
        'editMode' => $article->getId() ==! null]);
    }

    /**
    *  @Route("/admin/{id}/remove", name="remove_article")
    */
    public function remove(Article $article) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($article);
        $em->flush();


        return $this->redirectToRoute("homepage", []);
    }

    /**
    *  @Route("/admin/category-select", name="select_category")
    */
    public function selectCategory(CategoryRepository $repo, Request $request) {

        $categories = $repo->findAll();

        if ($request->isMethod('POST')) {

            $id = $request->get("category");
    
            return $this->redirectToRoute("edit_category", ["id" => $id]);
        }

        return $this->render("selectCategory.html.twig", ["categories" => $categories]);
    }

    /**
    *  @Route("/admin/category-select-remove", name="select_remove_category")
    */
    public function selectRemoveCategory(CategoryRepository $repo, Request $request) {

        $categories = $repo->findAll();

        if ($request->isMethod('POST')) {

            $id = $request->get("category");
    
            return $this->redirectToRoute("remove_category", ["id" => $id]);
        }

        return $this->render("selectRemoveCategory.html.twig", ["categories" => $categories]);
    }

    /**
    *  @Route("/admin/{id}/category-remove", name="remove_category")
    */
    public function removeCategory(Category $category) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute("select_remove_category", []);
    }

    /**
     *  @Route("/admin/add-category", name="add_category")
     *  @Route("/admin/{id}/edit-category", name="edit_category")
     */
    public function category(Category $category = null, Request $req, ObjectManager $manager){

        if(!$category) {
        $category = new Category();
        }

        $form = $this->createFormBuilder($category)
                     ->add("title")
                     ->getForm();

        $form->handleRequest($req);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($category);
            $manager->flush();

            return $this->redirectToRoute("create_article", []);
        }
        
        return $this->render("category.html.twig", ["formCategory" => $form->createView()]);
    }


    /**
     *  @Route("/{id}", name="show_article")
     */
    public function show(Article $article, $id, Request $request, ObjectManager $manager){

        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $comment->setCreatedAt(new \DateTime())
                    ->setArticle($article);

            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute("show_article", ["id" => $article->getId()]);
        }

        return $this->render("show.html.twig", ["article" => $article, "commentForm" => $form->createView(), "imageURI" => $this->getParameter('images_URI')]);
    }

    /**
    *  @Route("/admin/{id}/edit-comment", name="edit_comment")
    */
    public function editComment(Comment $comment, Request $request, ObjectManager $manager) {

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $manager->persist($comment);
            $manager->flush();

            return $this->redirectToRoute("show_article", ["id" => $comment->getArticle()->getId()]);
        }

        return $this->render("editComment.html.twig", ["commentEditForm" => $form->createView()]);
    }

    /**
    *  @Route("/admin/{id}/remove-comment", name="remove_comment")
    */
    public function removeComment(Comment $comment) {

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($comment);
        $em->flush();

        return $this->redirectToRoute("homepage", []);
    }

}

?>