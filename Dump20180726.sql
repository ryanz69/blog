-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: blog
-- ------------------------------------------------------
-- Server version	10.1.30-MariaDB-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (7,'ryan.zegar.01@gmail.com','ryan','$2y$13$/JfO5A2Z.8F9Aw5y7xLV3uSGrxGtkx5nQvIc0fvX8CtTYEefrWwcG');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resume` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_23A0E6612469DE2` (`category_id`),
  CONSTRAINT `FK_23A0E6612469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (15,'Molestiae ipsa voluptas','Ipsum cum sit et molestiae. Consectetur modi quas eveniet accusantium. Earum voluptatem sit consectetur.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Voluptatibus consequuntur quod aut eaque ipsam sequi. Est quo repudiandae dolorem fugit corrupti. Nulla reiciendis dolores enim necessitatibus iste cum. Et modi ad sequi et ratione molestiae nobis.</p><p>Necessitatibus error et voluptatem porro libero. Consequatur nulla quis dolore voluptatem provident repellat. Ut error omnis enim similique ea.</p><p>Aliquam ea alias ut ea vero adipisci et. Et ut aut necessitatibus aut nam culpa. Dolor molestiae sit vel ea.</p><p>Quia aliquid ad nostrum natus. Nam aut quia iusto labore non quo impedit sit. Delectus voluptatem nihil aut blanditiis. Repudiandae est ratione nihil sit a magnam.</p><p>Eum voluptate repellat labore velit unde. Cumque dolor molestiae voluptatem aut. Provident qui ut suscipit rerum quae. Quo impedit eos error consequuntur. Quaerat repellendus inventore nihil velit eum consequatur magnam.</p>','1975-04-12 07:50:01',3),(16,'Qui id ex perspiciatis','Velit eum quasi quae nihil eveniet in. Asperiores illum quibusdam accusantium alias voluptatem et sunt voluptatum. Ut minima est porro nobis repudiandae recusandae.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Dicta rerum nam et quos impedit inventore nihil. Libero nesciunt voluptatibus odit quas sequi voluptas ea. Eius natus ex in veniam esse ipsam corporis. Inventore laudantium eius pariatur qui deleniti et ut.</p><p>Ut facere sunt aut et. Facilis quo sed repudiandae eveniet. Qui amet quia eveniet consequatur commodi ad dolore.</p><p>Commodi eius voluptatibus quidem blanditiis eum ratione asperiores. Adipisci quod ipsam harum itaque. Minus sed non delectus consequuntur dignissimos quidem facilis.</p><p>Odit voluptates vel ipsum ab repellat saepe ipsum. Necessitatibus consequuntur ea voluptatibus eligendi. Voluptatem perspiciatis quas et rerum incidunt illo unde et.</p><p>Rerum alias laborum dolores perferendis id. Aut dolore sit atque distinctio ipsa optio unde. Facere aspernatur sed maxime ut voluptatem in consequatur. Quis officiis ut qui nemo tempora omnis.</p>','1999-03-23 06:02:48',1),(17,'Voluptatem voluptas non vitae et aut quisquam.','Voluptatem quo aut reprehenderit rerum autem minus adipisci. Eius vel blanditiis id sed. Rem asperiores et cum. Quaerat consequuntur suscipit distinctio praesentium.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Nesciunt qui rerum voluptatem quisquam. Optio qui quae dolore sunt. Cupiditate voluptatem velit perspiciatis et sed.</p><p>Et maiores totam sunt pariatur asperiores voluptatem nesciunt. Quia delectus et placeat dolor occaecati aut eum. Ullam cupiditate enim ut sunt aperiam iste ut. Deleniti debitis sint nesciunt maiores ab quia ut.</p><p>Labore sit illum maxime perferendis ipsa. Occaecati aut nulla dolore est exercitationem quisquam ut. Harum totam quod doloribus quod quis.</p><p>Ex quae commodi occaecati ut. Minima non sint aut voluptatem similique nesciunt quod. Culpa vitae corporis nobis enim nihil. Quam harum omnis fugiat enim doloremque consequatur.</p><p>Voluptatem aspernatur similique aut et quia omnis. Nam temporibus est aut ad. Autem sunt iste libero voluptatem.</p>','1988-12-02 00:35:45',1),(18,'Repellendus dolores nemo quasi voluptatem sunt nihil.','Omnis corrupti labore ab ex. Ipsam quae possimus voluptas porro cupiditate. Porro et ut provident rerum.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Sequi soluta optio optio dolore enim ipsam ex. Neque est numquam sunt veritatis omnis qui aperiam itaque. Enim qui beatae consequatur et eos impedit praesentium consequuntur. Mollitia architecto hic explicabo iusto. Ratione magni sunt suscipit et similique.</p><p>Ab quas ducimus perferendis repellat provident quia. Perferendis ut nam ipsam rerum repellendus totam nemo. Et ipsum ipsa cupiditate omnis. Qui voluptatem consectetur voluptatem assumenda consequuntur iste. Est recusandae tempora earum voluptatibus dolorem et quos voluptate.</p><p>Magni libero nulla autem ex in corrupti. Nesciunt omnis non officiis voluptas debitis.</p><p>Hic libero saepe corporis et quo quisquam quisquam. Laboriosam incidunt temporibus incidunt mollitia. Voluptatem odio nostrum quos minus consequuntur excepturi provident occaecati. Atque libero consequatur et dolores id voluptate fugit.</p><p>Ipsum libero fuga eaque. Voluptatem aperiam minus laudantium corrupti. Occaecati quam facere iure fugiat aut.</p>','1991-04-27 10:32:19',2),(19,'Dolores optio repellat dicta id consequatur corrupti reiciendis culpa.','Quibusdam blanditiis et quibusdam. Nesciunt quibusdam et ipsum labore. Animi hic soluta sapiente quod est ad. Alias aut nisi vero quos unde sunt consequuntur.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Sed blanditiis autem animi sed unde accusamus quod. Sunt a magnam vitae molestiae cumque aut delectus. Rem dolores adipisci debitis minus laborum voluptas. Minus qui in odit velit dolore eum. Corrupti incidunt autem harum illo dolores numquam.</p><p>Commodi voluptas quos est nobis nulla modi. Doloribus et nulla eum sapiente nihil molestias.</p><p>At est adipisci deleniti veniam. Vel odit unde temporibus quis. Quod eum voluptatibus accusantium quia.</p><p>Doloribus neque aspernatur adipisci laboriosam sit non non. Quia reprehenderit veniam suscipit cumque cumque doloremque. Voluptatum autem facere rerum provident aut voluptatem quia sint. Consectetur vel assumenda quam quia omnis dicta. Eum eos magnam omnis cum maxime eaque voluptate.</p><p>Quo consequatur aut est ex vel numquam. Deleniti sit reiciendis sapiente corporis. Quia error aliquid nemo et dolores sunt ex. Earum quia magnam a ut perferendis dolor delectus.</p>','2010-06-01 17:37:52',2),(20,'Quia sit veritatis est omnis.','In ut earum qui commodi. Iusto unde aut nostrum eaque. Aliquid quam dignissimos placeat voluptatum modi. Est sit fugiat perferendis fuga et delectus.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Porro quibusdam aut qui omnis rerum et. Minima consequatur et voluptates asperiores.</p><p>Eaque molestiae iusto aut voluptates. Veritatis asperiores doloremque qui autem nihil et exercitationem. Mollitia excepturi quis est cupiditate.</p><p>Voluptas inventore nostrum nihil quibusdam repellendus officiis. A facere repellat consequatur sequi sequi qui. Vitae beatae nam deserunt incidunt impedit. Dolores sit et nostrum.</p><p>Error dolores consequatur at ab et et. Voluptatem dolor sed earum magni consequuntur sed consequatur. Animi corrupti sit consequatur et suscipit dignissimos vel voluptas.</p><p>Et officia eveniet enim ea. Sint temporibus harum dolores omnis ea earum molestiae eos. Dicta aut nemo ullam quis alias dolores et. Distinctio fugiat eos et aperiam. Dolore et sit non eos.</p>','2006-11-28 20:08:50',2),(21,'Et voluptas repellendus quidem voluptas quos quis dolorem.','Qui enim officiis magni ipsum unde. Velit veritatis nemo ea qui quidem. Et autem saepe similique dolorem amet alias.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Dolorem voluptatem aliquam sit odit. Officiis est nam ut enim. Vel inventore sed laudantium quae omnis quas eum. Maxime incidunt mollitia sed minus ad.</p><p>Sunt et dignissimos cumque qui omnis quis asperiores dolorem. Consequatur sit ad voluptas saepe aut officiis. Et ipsa at consequatur voluptatem.</p><p>Et non eaque voluptatibus vero reprehenderit. Veniam atque voluptatem esse ut debitis aperiam numquam dolore. Delectus est ut hic sed consequatur.</p><p>Fuga molestias ipsam et dolorem rerum quod maxime. Dicta reiciendis deleniti aliquid ducimus facilis id voluptas ut. Saepe fugiat libero ipsum iste minima et.</p><p>Suscipit reiciendis laborum et labore et animi. Et animi necessitatibus aut reprehenderit enim odio. Sit deserunt sit maiores et. Eos dolorem sed et rerum maxime.</p>','2015-07-02 08:10:47',3),(22,'Cum totam architecto molestiae quasi qui et illum illo.','Ab id nam placeat tenetur nulla. Cupiditate assumenda dolorem voluptate sapiente at assumenda aliquam assumenda. Ipsum pariatur sint eum eius.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Qui exercitationem delectus eius at. Quisquam est perferendis fugiat ut ratione. Iste et veritatis iure ut nulla sit asperiores. Alias voluptatem sed dolor aut deserunt rerum quod.</p><p>Quod molestiae est non velit. Adipisci esse molestiae nihil officiis repellendus quibusdam veritatis. Aut expedita eius sunt.</p><p>Quia rerum aliquid laudantium perspiciatis. Velit id sit aut ullam odit.</p><p>Magnam reiciendis eos suscipit soluta quas omnis. Et tempore voluptates cupiditate facilis officiis iste. Magni quisquam voluptatibus quae quas consequatur ad.</p><p>Repellendus vel voluptatem accusamus dolores. Beatae possimus aliquid reiciendis ipsa et nisi quos nihil. Cum temporibus rerum sit maxime.</p>','1998-08-02 18:46:16',3),(23,'Quisquam quo omnis quis est quod.','Vero numquam tenetur in quasi. Dolor nesciunt odit sunt consequatur illum ut. Aut exercitationem praesentium aperiam id eos et ut. Aut ullam quia repellat neque.','3f96f5e90005bebf2adebe8c92a27c3e.png','<p>Animi cumque cupiditate non omnis. Voluptas omnis quae nostrum quo voluptas. Minus minima ipsam dolores aliquid quas nisi.</p><p>Necessitatibus ullam eligendi voluptates sed illo. Aut sapiente praesentium sed architecto quia. Qui consectetur esse qui quo est aspernatur dolorem. Iste aut quas provident autem illo.</p><p>Quis quia minima fugit deleniti. Quas placeat quam consequuntur quae quia blanditiis sed. Dolor voluptatem veritatis laborum harum accusantium enim. Numquam sapiente cupiditate est esse.</p><p>Corporis dignissimos inventore praesentium ipsa quo. Laborum vel dolor dolorem incidunt enim ducimus voluptatem. Amet corporis culpa mollitia qui quae perspiciatis quia.</p><p>Ex et quas illo eum deserunt et laborum asperiores. Qui et qui numquam repellat consequatur incidunt temporibus. Accusantium inventore laborum unde qui ea voluptas.</p>','2009-08-20 03:30:17',3),(30,'testImagesUpload','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','2f287a505b2c7705e24330283066934c.png','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','2018-07-26 14:09:54',4),(31,'testCategory','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','adaf6b5f4e12d5bf1c7ee31af6274118.png','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa','2018-07-26 14:40:02',1);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Vitae dignissimos asperiores eligendi voluptatibus non amet sit.'),(2,'Aspernatur perspiciatis repellendus esse suscipit fugiat sunt.'),(3,'Quisquam sapiente ut voluptas ut eaque ut.'),(4,'edit'),(5,'newEdit');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526C7294869C` (`article_id`),
  CONSTRAINT `FK_9474526C7294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (3,15,'Manon Louis','<p>Sed sit ratione nobis a enim quis est aspernatur. Temporibus aspernatur voluptates voluptatum non dolor id possimus. Voluptatem temporibus eum at laudantium est.</p><p>Corporis perferendis animi consequatur dolore. Sequi qui saepe quia sequi.</p>','1993-09-13 17:55:39','celine.delmas@club-internet.fr'),(5,16,'Alexandrie de Bertrand','<p>Iste sit blanditiis quia laudantium. Cumque unde temporibus repudiandae alias. Est quas quaerat voluptas ea.</p><p>Sunt perferendis sint voluptatem qui. Voluptatum necessitatibus quas illum suscipit ut.</p>','2013-09-12 10:28:47','pelletier.simone@gmail.com'),(6,17,'Cécile Sanchez','<p>Sunt eius enim suscipit unde voluptates labore dolores. Sunt et deleniti quis non quidem earum. Neque possimus voluptas asperiores. Perferendis fuga minima esse et quod.</p><p>Libero molestias fugit illo autem. Enim architecto quidem tenetur consectetur. Unde cum at quia voluptatibus ut. Cum expedita asperiores distinctio est explicabo.</p>','2006-03-01 20:48:22','kduval@bruneau.net'),(7,17,'Eugène Barbier','<p>Itaque voluptatem molestiae ex omnis saepe quod. Repellat incidunt minus reprehenderit saepe impedit dolor ut quo.</p><p>Vitae similique quam ut soluta. Omnis quo tempora et ipsum accusantium. Placeat laudantium provident maiores quos aut enim veritatis.</p>','2007-09-17 01:44:14','david49@noos.fr'),(8,17,'Antoinette Bodin','<p>Repellendus aut consequatur quisquam cumque qui eum. Esse animi qui expedita dignissimos non quam est. Eaque quia qui facere quo et provident impedit.</p><p>Tenetur nihil consequatur quibusdam voluptatem rerum necessitatibus animi. Omnis dicta accusamus rem magnam deserunt ut ut. Non fugit quia vero nulla provident voluptates vitae animi. Voluptate officia sapiente tempora ut odio et est exercitationem.</p>','1993-12-23 07:28:34','vvalette@bertin.com'),(9,18,'Louis-Guy Gomes','<p>Dolore vel at repellat temporibus voluptatem. Non et recusandae consectetur hic. Excepturi sed mollitia magni est. Explicabo voluptatem et qui magnam ipsam vel rem. Molestias molestias minus reiciendis fugiat dignissimos accusamus sed quasi.</p><p>Perferendis ipsa eius ut hic praesentium. Officiis provident ut ad dolor qui eveniet. Tempora libero eius iure magni ut. Mollitia id iusto enim et aut.</p>','2017-08-07 11:06:47','mercier.adele@lebreton.net'),(10,18,'Claire Texier','<p>Cum dignissimos neque qui dolor magni aut quia. Deserunt veniam et et laborum non provident non nisi. Tempore quas eos quae ab cupiditate facilis similique et.</p><p>Quia velit qui quae autem eius ut et. Exercitationem autem non qui ut omnis repudiandae ut. Ea rerum reprehenderit esse voluptas unde. Porro consequatur est voluptates laudantium impedit perferendis.</p>','1991-10-03 11:47:41','michelle19@orange.fr'),(11,19,'Claude-Thibaut Chauvet','<p>Perferendis nulla eveniet est dicta ad. Id rerum veniam eos ullam. Nulla eos consectetur et iure nulla natus.</p><p>Optio aliquam blanditiis et aut qui soluta unde. Eius fugit qui eos vel corporis velit. Magnam sint eos repellendus aspernatur numquam. Nam ipsum dolores ea totam aspernatur qui itaque quis.</p>','2017-02-02 21:11:41','isaac36@gmail.com'),(12,19,'Clémence Petit','<p>Voluptatem inventore eos voluptate ipsam. Quia inventore illo illum soluta odio illum. Non sit totam rerum unde cum. Quis esse autem quae vel adipisci voluptatem officiis.</p><p>Modi ullam reprehenderit inventore reprehenderit. Accusamus tempora harum praesentium nobis laboriosam molestiae illum. Ea consequatur odit ut aliquid. Facere accusamus qui vitae ullam ea impedit expedita unde.</p>','2017-04-25 19:47:07','danielle.thibault@hotmail.fr'),(13,20,'Frédéric du Lejeune','<p>Alias nemo sint laboriosam quis. Ea exercitationem excepturi expedita quam. Et ea qui nam non. Quas omnis et harum autem quia eius nisi. Ut et dolore eos eveniet ea unde.</p><p>Minus consequatur eveniet a placeat aut molestiae. Exercitationem et enim fugiat et rem libero. Ipsa et sit quibusdam impedit. Id aut ut in dolore ad ut.</p>','2012-07-06 19:47:08','michaud.laurence@dbmail.com'),(14,20,'Dominique-Jeannine Louis','<p>Sit corrupti quo cupiditate voluptatem voluptates sit quisquam. Praesentium quas dolor vel velit nostrum minima. Nam et qui voluptas nobis.</p><p>Quibusdam error maiores quas vel deserunt sunt expedita assumenda. Maxime ut excepturi nulla. Voluptas deserunt et sunt temporibus enim. Voluptas rerum blanditiis corporis expedita.</p>','2013-10-28 04:08:13','mguillot@masson.net'),(15,21,'Éric Ollivier','<p>Quis consequuntur harum qui fuga at. Iusto doloribus consequuntur neque fugiat. Sit est cumque voluptas rem natus praesentium. Rerum iste pariatur et eos ipsa impedit earum. Non nesciunt exercitationem incidunt sed et.</p><p>Repellat dolores saepe eius ipsum non. Necessitatibus libero eius voluptate minus vel id dignissimos. Assumenda ipsam ea sunt provident. In exercitationem cupiditate veritatis repudiandae.</p>','2016-08-30 17:29:42','lesage.nath@gmail.com'),(16,22,'Émile-Philippe Maury','<p>Debitis perspiciatis et ut dolorem nam sequi. Dolore nihil quia quia omnis sint amet. Dolores id omnis et quia exercitationem placeat. Autem placeat at laudantium aliquid aliquid molestiae sequi.</p><p>Veniam a at sunt nam commodi sunt corrupti. Sunt quis quod odit aut. Nobis et veritatis adipisci et asperiores at. Consequatur necessitatibus necessitatibus error molestiae itaque doloremque illo.</p>','2017-05-30 10:31:41','timothee.alves@leclercq.fr'),(17,22,'Guy Henry-Carlier','<p>Ratione illo quam vitae. Quia ea sunt labore omnis corrupti porro. Illo eos eos repellat et quas harum ut nesciunt. Ut laborum quia aperiam.</p><p>Accusamus voluptas tenetur praesentium perspiciatis natus. Et est ipsum harum qui. Corrupti impedit eum qui libero aperiam magnam. Ut est sed officia delectus necessitatibus eaque.</p>','2005-04-27 12:26:23','duhamel.penelope@wanadoo.fr'),(18,23,'Bernadette Simon','<p>Aut labore exercitationem ducimus sit deleniti quis quia id. Minus neque expedita quo ea non. Voluptas voluptas unde eligendi exercitationem et aspernatur voluptas. Ratione totam labore expedita qui.</p><p>Perspiciatis quia quam minus et facilis in quae. Reiciendis nemo recusandae aut est labore odio libero dolores. Quam omnis nobis iure dolor officiis aliquam temporibus. Hic ipsam repellendus labore quos explicabo quas molestiae. Vitae ut labore maiores accusamus qui.</p>','2013-09-12 19:59:00','sreynaud@hoarau.fr'),(19,23,'Philippine Raymond','<p>Soluta unde sit ut numquam. Consequatur aut tenetur corporis debitis quisquam ducimus ut. Fuga porro perspiciatis nihil deleniti dolor amet. Laudantium recusandae amet omnis quasi rerum sed recusandae.</p><p>Quis quia atque voluptatem culpa debitis autem. Quis minima exercitationem aut sit et. Praesentium commodi facilis eaque ab ducimus quam blanditiis aspernatur. In aut commodi error culpa est reiciendis beatae.</p>','2014-07-21 10:17:41','noel.auguste@laurent.com');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20180713125823'),('20180718140352'),('20180718141442'),('20180720122439'),('20180720133624');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 15:52:49
